import sqlite3 as sq
import random as r
from people_data import all_name, all_mail, all_jobs


#створить базу даних (довільне ім'я)
connection = sq.connect('hw_5_base.db')
cursor = connection.cursor()

#Створить таблицю people з атрибутами
cursor.execute("""
    create table people(
       id integer primary key autoincrement not null,
       first_name text not null,
       last_name text not null,
       sex text not null,
       salary int,
       position text,
       email text,
       age int not null);
    """)


#Згенерує 20 записів на ваш смак причому для тих полів які не обов'язкові вказувати вказувати дані вибирково наприклад з
#20 тільки 5 мають email, 10 посада - тощо а зарплата і вік генерується за допомогою функції рандом
for x in range(1,21):
    name = all_name[x-1]
    f_name = name.split(" ")[1]
    l_name = name.split(" ")[2]
    sex = name.split(" ")[0]
    age = r.randint(18, 56)
    salary = r.randint(5000, 28000)
    if x % 4 == 0:
        job = r.choice(all_jobs)
        mail = r.choice(all_mail)
        cursor.execute(f"""
            insert into people values
            ({x},'{f_name}', '{l_name}', '{sex}', {salary}, '{job}', '{mail}', {age});""")
    elif x % 4 != 0 and x % 2 == 0:
        job = r.choice(all_jobs)
        cursor.execute(f"""
            insert into people (first_name, last_name, sex, salary, position, age)
            values ('{f_name}', '{l_name}', '{sex}', {salary}, '{job}', {age});""")
    else:
        cursor.execute(f"""
             insert into people (first_name, last_name, sex, salary, age)
             values('{f_name}', '{l_name}', '{sex}', {salary}, {age});""")


#Додати два записи для Laurence Wachowski та Andrew Wachowski стать чоловік інші дані довільні
cursor.execute(f"""
            insert into people (first_name, last_name, sex, salary, position, age)
            values('Laurence','Wachowski','man', {r.randint(5000, 28000)},
            '{r.choice(all_jobs)}',{r.randint(18, 56)});
            """)

cursor.execute(f"""
            insert into people (first_name, last_name, sex, salary, position, age)
            values('Andrew', 'Wachowski', 'man', {r.randint(5000, 28000)},
            '{r.choice(all_jobs)}' ,{r.randint(18, 56)});
            """)


#Змінити стать для Laurence Wachowski та Andrew Wachowski на жінка
cursor.execute("UPDATE people SET sex = 'woman' where last_name = 'Wachowski';")


#Змінити ім'я для Laurence Wachowski на Lana
#Змінити ім'я для Andrew Wachowski на Lilly
cursor.execute("UPDATE people SET first_name = 'Lana' where first_name = 'Laurence' and last_name = 'Wachowski';")
cursor.execute("UPDATE people SET first_name = 'Lilly' where first_name = 'Andrew' and last_name = 'Wachowski';")


#додати для всіх записів в яких немає email - email який складатиместья з імені і прізвища та довільний хост
cursor.execute("UPDATE people SET email = first_name || '.' || last_name || '@gmail.com' where email is null;")


#Вивести всіх людей з зарплатою більще 10000
rich_people = cursor.execute("SELECT * from people where salary > 10000;")
print(f"\nЛюди, у яких зарплата більше 10000:")
for row in rich_people:
    print(row)
print('*' * 80)


#Вивести всіх людей з зарплатою 10000 віком старше 25
old_rich_people = cursor.execute("SELECT * from people where salary > 10000 and age > 25;")
print(f"\nЛюди, у яких зарплата більше 10000 так вiк страше 25 рокiв:")
for row in old_rich_people:
    print(row)
print('*' * 90)


#видалити всіх людей без посади
cursor.execute('DELETE from people where position is null')

#Остаточный вивід
working_people = cursor.execute("SELECT * from people;")
print(f"\nЛюди, якi мають роботу!")
for d in working_people:
    print(d)

cursor.close()
connection.commit()
connection.close()
