
all_name = [
    'man Bill Bringman', 'woman Linda Barker',  'man Jim Style', 'woman Melissa Pardi', 'man Geralt Rivskiy',
    'man Axel Spirits', 'man Billy Rogers', 'woman Maria Keller', 'man Jak Vasovski', 'man Paul Moldec','man Ringo Star',
    'man Ally Baba', 'man Nicky Nelson', 'woman Ariana Gremmi','man Carl Simpson', 'woman Lady Gaga','man Arthur King',
    'man Laurence Versachi', 'woman Janet Larson','man Pedro Paskal']

all_mail = [
    'wachows@gmail.com', 'Bar@mail.ru',  'Jim@ukr.ua', 'RoPdi@gmail.com', 'Wacski@mail.com', 'gfrits@ukr.net',
    'biLrgers@mail.ru', 'chaeller@gmail.com', 'jaVasski@mail.ru', 'pac@gmail.ru','ringstar@ukr.net', 'allybaa@gmail.com',
    'nickson@mail.ru', 'AGro@ukr.net','Campson@gmail.com', 'laaga@mail.ru','arKing@gmail.com','Larsachi@gmail.com',
    'janLaon@mail.ru','pekal@gmail.com']

all_jobs = [
    'accountant', 'actor',  'actress', 'artist', 'astronaut', 'auditor',
    'baker', 'banker', 'carpenter', 'farmer','programmer', 'singer',
    'waiter', 'painter','engineer', 'doctor','coach','carpenter',
    'butcher','hairstylist']